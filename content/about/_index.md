---
title: About me
subtitle: Deborah Balm - Voice Artist
comments: false
---

My name is Deborah Balm. I work as a vocal artist specialising in:

- Audiobooks.  Fiction or factual, I enjoy a wide variety.
- Education and eLearning with a good understanding of tech content and concepts.
- Self-improvement manuals
- Established Librivox Track record


### Audiobook Credits
Emma's Dance: The Second Book in the Dance Series
By Leslie Hachtel

The Dream Dancer: The First Book in the Dance Series
By Leslie Hachtel

5-Minute Practice To Good Self-Esteem Habits: Regain Confidence & Strengthen Esteem
By Elizabeth Caroline

### Prior History
Before starting to record audiobooks in earnest I recorded a good number of chapters for Librivox.
It was my experience with Librivox that convinced me that this was the direction I wanted to go in!
Just see [my reader page](https://librivox.org/reader/11447) 
